package pro.eugw.NullCore;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.yaml.snakeyaml.Yaml;

import java.io.*;
import java.util.Map;

class Misc {

    static Logger log() {
        return LogManager.getLogger();
    }

    static Map getConfig() {
        Yaml yaml = new Yaml();
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream("nullcore.yml");
        } catch (FileNotFoundException e) {
            log().error("Configuration not found");
        }
        return (Map) yaml.load(inputStream);
    }

}

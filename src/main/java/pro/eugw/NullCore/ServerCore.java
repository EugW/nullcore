package pro.eugw.NullCore;

import com.github.cage.Cage;
import com.github.cage.image.Painter;
import com.github.steveice10.mc.auth.data.GameProfile;
import com.github.steveice10.mc.protocol.MinecraftConstants;
import com.github.steveice10.mc.protocol.MinecraftProtocol;
import com.github.steveice10.mc.protocol.ServerLoginHandler;
import com.github.steveice10.mc.protocol.data.game.chunk.BlockStorage;
import com.github.steveice10.mc.protocol.data.game.chunk.Chunk;
import com.github.steveice10.mc.protocol.data.game.chunk.Column;
import com.github.steveice10.mc.protocol.data.game.chunk.NibbleArray3d;
import com.github.steveice10.mc.protocol.data.game.entity.EquipmentSlot;
import com.github.steveice10.mc.protocol.data.game.entity.metadata.ItemStack;
import com.github.steveice10.mc.protocol.data.game.entity.metadata.Position;
import com.github.steveice10.mc.protocol.data.game.entity.player.GameMode;
import com.github.steveice10.mc.protocol.data.game.setting.Difficulty;
import com.github.steveice10.mc.protocol.data.game.world.WorldType;
import com.github.steveice10.mc.protocol.data.game.world.block.BlockState;
import com.github.steveice10.mc.protocol.data.game.world.map.MapData;
import com.github.steveice10.mc.protocol.data.game.world.map.MapIcon;
import com.github.steveice10.mc.protocol.data.message.TextMessage;
import com.github.steveice10.mc.protocol.packet.ingame.client.ClientChatPacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.player.ClientPlayerChangeHeldItemPacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.player.ClientPlayerPositionRotationPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerChatPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerJoinGamePacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.ServerEntityEquipmentPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.player.ServerPlayerAbilitiesPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.player.ServerPlayerChangeHeldItemPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.player.ServerPlayerPositionRotationPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.world.ServerChunkDataPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.world.ServerMapDataPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.world.ServerSpawnPositionPacket;
import com.github.steveice10.packetlib.Server;
import com.github.steveice10.packetlib.Session;
import com.github.steveice10.packetlib.event.server.ServerAdapter;
import com.github.steveice10.packetlib.event.server.SessionAddedEvent;
import com.github.steveice10.packetlib.event.session.PacketReceivedEvent;
import com.github.steveice10.packetlib.event.session.SessionAdapter;
import com.github.steveice10.packetlib.tcp.TcpSessionFactory;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Arrays;

import static pro.eugw.NullCore.Misc.getConfig;
import static pro.eugw.NullCore.Misc.log;

public class ServerCore extends Thread {

    private Server server;
    private String host;
    private Integer port;

    ServerCore(String host1, Integer port1) {
        host = host1;
        port = port1;
    }

    private String getNameCaptcha(String playerName) {
        for (Session s : server.getSessions()) {
            GameProfile profile = s.getFlag("profile");
            if (profile.getName().equals(playerName)) {
                return s.getFlag("cap");
            }
        }
        return "0";
    }

    private void getCaptcha(String cap, Session session) {
        Painter painter = new Painter(128, 128, Color.BLACK, null, null, null);
        Cage cage = new Cage(painter, null, null, null, 0.5F, null, null);
        cage.draw(cap);
        BufferedImage img = cage.drawImage(cap);
        Graphics2D graphics = img.createGraphics();
        graphics.drawImage(img, 0, 0, null);
        graphics.dispose();
        int[] pixels = new int[img.getWidth() * img.getHeight()];
        img.getRGB(0, 0, img.getWidth(), img.getHeight(), pixels, 0, img.getWidth());
        byte[] result = new byte[img.getWidth() * img.getHeight()];
        for (int i = 0; i < pixels.length; i++) {
            Color color = new Color(pixels[i], true);
            int index = 0;
            double best = -1;
            Color[] colors = new Color[]{new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0), new Color(89, 125, 39), new Color(109, 153, 48), new Color(127, 178, 56), new Color(67, 94, 29), new Color(174, 164, 115), new Color(213, 201, 140), new Color(247, 233, 163), new Color(130, 123, 86), new Color(140, 140, 140), new Color(171, 171, 171), new Color(199, 199, 199), new Color(105, 105, 105), new Color(180, 0, 0), new Color(220, 0, 0), new Color(255, 0, 0), new Color(135, 0, 0), new Color(112, 112, 180), new Color(138, 138, 220), new Color(160, 160, 255), new Color(84, 84, 135), new Color(117, 117, 117), new Color(144, 144, 144), new Color(167, 167, 167), new Color(88, 88, 88), new Color(0, 87, 0), new Color(0, 106, 0), new Color(0, 124, 0), new Color(0, 65, 0), new Color(180, 180, 180), new Color(220, 220, 220), new Color(255, 255, 255), new Color(135, 135, 135), new Color(115, 118, 129), new Color(141, 144, 158), new Color(164, 168, 184), new Color(86, 88, 97), new Color(106, 76, 54), new Color(130, 94, 66), new Color(151, 109, 77), new Color(79, 57, 40), new Color(79, 79, 79), new Color(96, 96, 96), new Color(112, 112, 112), new Color(59, 59, 59), new Color(45, 45, 180), new Color(55, 55, 220), new Color(64, 64, 255), new Color(33, 33, 135), new Color(100, 84, 50), new Color(123, 102, 62), new Color(143, 119, 72), new Color(75, 63, 38), new Color(180, 177, 172), new Color(220, 217, 211), new Color(255, 252, 245), new Color(135, 133, 129), new Color(152, 89, 36), new Color(186, 109, 44), new Color(216, 127, 51), new Color(114, 67, 27), new Color(125, 53, 152), new Color(153, 65, 186), new Color(178, 76, 216), new Color(94, 40, 114), new Color(72, 108, 152), new Color(88, 132, 186), new Color(102, 153, 216), new Color(54, 81, 114), new Color(161, 161, 36), new Color(197, 197, 44), new Color(229, 229, 51), new Color(121, 121, 27), new Color(89, 144, 17), new Color(109, 176, 21), new Color(127, 204, 25), new Color(67, 108, 13), new Color(170, 89, 116), new Color(208, 109, 142), new Color(242, 127, 165), new Color(128, 67, 87), new Color(53, 53, 53), new Color(65, 65, 65), new Color(76, 76, 76), new Color(40, 40, 40), new Color(108, 108, 108), new Color(132, 132, 132), new Color(153, 153, 153), new Color(81, 81, 81), new Color(53, 89, 108), new Color(65, 109, 132), new Color(76, 127, 153), new Color(40, 67, 81), new Color(89, 44, 125), new Color(109, 54, 153), new Color(127, 63, 178), new Color(67, 33, 94), new Color(36, 53, 125), new Color(44, 65, 153), new Color(51, 76, 178), new Color(27, 40, 94), new Color(72, 53, 36), new Color(88, 65, 44), new Color(102, 76, 51), new Color(54, 40, 27), new Color(72, 89, 36), new Color(88, 109, 44), new Color(102, 127, 51), new Color(54, 67, 27), new Color(108, 36, 36), new Color(132, 44, 44), new Color(153, 51, 51), new Color(81, 27, 27), new Color(17, 17, 17), new Color(21, 21, 21), new Color(25, 25, 25), new Color(13, 13, 13), new Color(176, 168, 54), new Color(215, 205, 66), new Color(250, 238, 77), new Color(132, 126, 40), new Color(64, 154, 150), new Color(79, 188, 183), new Color(92, 219, 213), new Color(48, 115, 112), new Color(52, 90, 180), new Color(63, 110, 220), new Color(74, 128, 255), new Color(39, 67, 135), new Color(0, 153, 40), new Color(0, 187, 50), new Color(0, 217, 58), new Color(0, 114, 30), new Color(91, 60, 34), new Color(111, 74, 42), new Color(129, 86, 49), new Color(68, 45, 25), new Color(79, 1, 0), new Color(96, 1, 0), new Color(112, 2, 0), new Color(59, 1, 0), new Color(147, 124, 113), new Color(180, 152, 138), new Color(209, 177, 161), new Color(110, 93, 85), new Color(112, 57, 25), new Color(137, 70, 31), new Color(159, 82, 36), new Color(84, 43, 19), new Color(105, 61, 76), new Color(128, 75, 93), new Color(149, 87, 108), new Color(78, 46, 57), new Color(79, 76, 97), new Color(96, 93, 119), new Color(112, 108, 138), new Color(59, 57, 73), new Color(131, 93, 25), new Color(160, 114, 31), new Color(186, 133, 36), new Color(98, 70, 19), new Color(72, 82, 37), new Color(88, 100, 45), new Color(103, 117, 53), new Color(54, 61, 28), new Color(112, 54, 55), new Color(138, 66, 67), new Color(160, 77, 78), new Color(84, 40, 41), new Color(40, 28, 24), new Color(49, 35, 30), new Color(57, 41, 35), new Color(30, 21, 18), new Color(95, 75, 69), new Color(116, 92, 84), new Color(135, 107, 98), new Color(71, 56, 51), new Color(61, 64, 64), new Color(75, 79, 79), new Color(87, 92, 92), new Color(46, 48, 48), new Color(86, 51, 62), new Color(105, 62, 75), new Color(122, 73, 88), new Color(64, 38, 46), new Color(53, 43, 64), new Color(65, 53, 79), new Color(76, 62, 92), new Color(40, 32, 48), new Color(53, 35, 24), new Color(65, 43, 30), new Color(76, 50, 35), new Color(40, 26, 18), new Color(53, 57, 29), new Color(65, 70, 36), new Color(76, 82, 42), new Color(40, 43, 22), new Color(100, 42, 32), new Color(122, 51, 39), new Color(142, 60, 46), new Color(75, 31, 24), new Color(26, 15, 11), new Color(31, 18, 13), new Color(37, 22, 16), new Color(19, 11, 8)};
            for (int i12 = 4; i12 < colors.length; i12++) {
                double raeann = (color.getRed() + colors[i12].getRed()) / 2.0;
                double r = color.getRed() - colors[i12].getRed();
                double g = color.getGreen() - colors[i12].getGreen();
                int b = color.getBlue() - colors[i12].getBlue();
                double weightR = 2 + raeann / 256.0;
                double weightG = 4.0;
                double weightB = 2 + (255 - raeann) / 256.0;
                double distance = weightR * r * r + weightG * g * g + weightB * b * b;
                if (distance < best || best == -1) {
                    best = distance;
                    index = i12;
                }
            }
            result[i] = (byte) (index < 128 ? index : -129 + (index - 127));
        }
        MapData md = new MapData(128, 128, 0, 0, result);
        session.send(new ServerEntityEquipmentPacket(0, EquipmentSlot.MAIN_HAND, new ItemStack(358)));
        session.send(new ServerMapDataPacket(0, (byte) 0, false, new MapIcon[0], md));
        byte[] biomes = new byte[256];
        Arrays.fill(biomes, (byte) ((Math.sin(0) + 1) * 10));
        Chunk[] chunk = new Chunk[16];
        for (int i = 0; i < 16; i++) {
            BlockStorage blocks = new BlockStorage();
            NibbleArray3d blockLight = new NibbleArray3d(16 * 16 * 16);
            NibbleArray3d skyLight = new NibbleArray3d(16 * 16 * 16);
            for (int a = 0; a < 16; a++) {
                for (int b = 0; b < 16; b++) {
                    for (int c = 0; c < 16; c++) {
                        if (i == 0 && b < 5) {
                            blocks.set(a, b, c, new BlockState(166, 0));
                        }
                        blockLight.set(a, b, c, 15);
                        skyLight.set(a, b, c, 15);
                    }
                }
            }
            chunk[i] = new Chunk(blocks, blockLight, skyLight);
        }
        session.send(new ServerChunkDataPacket(new Column(0, 0, chunk, biomes, null)));
    }

    @Override
    public void run() {
        server = new Server(host, port, MinecraftProtocol.class, new TcpSessionFactory());
        server.setGlobalFlag(MinecraftConstants.VERIFY_USERS_KEY, false);
        server.setGlobalFlag(MinecraftConstants.SERVER_LOGIN_HANDLER_KEY, (ServerLoginHandler) session -> {
            session.send(new ServerJoinGamePacket(0, false, GameMode.SURVIVAL, 0, Difficulty.PEACEFUL, 0, WorldType.DEFAULT, false));
            session.send(new ServerSpawnPositionPacket(new Position(5, 5, 5)));
            session.send(new ServerPlayerAbilitiesPacket(false, false, false, false, (float) 1, (float) 1));
            session.send(new ServerPlayerPositionRotationPacket(5,5,5,0,90, 0));
            new Thread(() -> {
                while (session.isConnected()){
                    session.send(new ServerChatPacket(new TextMessage(getConfig().get("prefix").toString().replace("&", "\u00a7") + "&fНапишите в чат &aцифры с картинки".replace("&", "§"))));
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        log().error(e);
                    }
                }
            }).start();
            StringBuilder code = new StringBuilder();
            for (int i = 1; i <= 4; i++)
                code.append((int) (Math.random() * ((9) + 1)));
            session.setFlag("cap", code.toString());
            getCaptcha(code.toString(), session);
            log().info("Total online: " + server.getSessions().size());
        });
        server.addListener(new ServerAdapter() {
            @Override
            public void sessionAdded(SessionAddedEvent event) {
                event.getSession().addListener(new SessionAdapter() {
                    @Override
                    public void packetReceived(PacketReceivedEvent event) {
                        if (event.getPacket() instanceof ClientPlayerPositionRotationPacket) {
                            event.getSession().send(new ServerPlayerPositionRotationPacket(5,5,5,0,90, 0));
                        }
                        if (event.getPacket() instanceof ClientPlayerChangeHeldItemPacket) {
                            event.getSession().send(new ServerPlayerChangeHeldItemPacket(4));
                        }
                        if (event.getPacket() instanceof ClientChatPacket) {
                            ClientChatPacket packet = event.getPacket();
                            GameProfile profile = event.getSession().getFlag(MinecraftConstants.PROFILE_KEY);
                            if (packet.getMessage().equals(getNameCaptcha(profile.getName()))) {
                                event.getSession().send(new ServerChatPacket(new TextMessage(getConfig().get("prefix").toString().replace("&", "\u00a7") + "&aВы успешно прошли проверку. Приятной игры!".replace("&", "§"))));
                            } else {
                                event.getSession().send(new ServerChatPacket(new TextMessage(getConfig().get("prefix").toString().replace("&", "\u00a7") + "&cЭто не верно. Попробуй ещё раз".replace("&", "§"))));
                            }
                        }
                    }
                });
            }
        });
        server.bind();
    }

}

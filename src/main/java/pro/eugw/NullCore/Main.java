package pro.eugw.NullCore;

import java.io.*;

import static pro.eugw.NullCore.Misc.getConfig;
import static pro.eugw.NullCore.Misc.log;

public class Main {

    private Main() {
        log().info("Initializing configuration...");
        File config = new File("nullcore.yml");
        if (!config.exists()) {
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/nullcore.yml")));
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(config)));
                String rd;
                while ((rd = reader.readLine()) != null) {
                    writer.write(rd);
                    writer.newLine();
                }
                reader.close();
                writer.flush();
                writer.close();
            } catch (Exception e) {
                log().error("Error while initializing configuration");
                log().error("Exit");
                System.exit(0);
            }
        }
        log().info("Configuration initialized successfully");
    }

    public static void main(String[] args) {
        log().info("Starting...");
        new Main();
        new ServerCore((String) getConfig().get("ip"), (Integer) getConfig().get("port")).start();
        log().info("Done");
    }

}